<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int|null $status
 * @property string $description
 * @property string $created_at
 * @property string|null $updated_at
 * @property string $beginning
 * @property string $ending
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'description', 'beginning', 'ending'], 'required'],
            [['status'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'beginning', 'ending'], 'safe'],
            ['ending', 'compare', 'compareAttribute' => 'beginning', 'operator' => '>',],
            [['name'], 'string', 'max' => 250],
            [['type'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'status' => 'Status',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'beginning' => 'Beginning',
            'ending' => 'Ending',
        ];
    }

    public function beforeSave($insert)
    {
        $this->beginning = date('Y-m-d H:i:s', strtotime($this->beginning));
        $this->ending = date('Y-m-d H:i:s', strtotime($this->ending));
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }
}
