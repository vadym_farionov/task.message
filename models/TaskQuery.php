<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Task]].
 *
 * @see Task
 */
class TaskQuery extends \yii\db\ActiveQuery
{
    const IN_WORK = 2;
    const DONE = 1;
    const PAUSED = 0;


    public function active($state = true)
    {
        return $this->andWhere(['status' => $state]);
    }

    public function statuses()
    {
        return [
            self::IN_WORK => 'In work',
            self::DONE => 'Done',
            self::PAUSED => 'Paused',
        ];
    }

    /**
     * {@inheritdoc}
     * @return Task[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Task|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
