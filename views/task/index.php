<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{summary}\n{pager}",
        'pager' => ['pagination' =>['pageSize' => 5]],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['label' => 'name',
            'value' => function ($model) {
                return Html::a(Html::encode($model->name), \yii\helpers\Url::to(['view', 'id' => $model->id]));
            },
            'format' => 'raw',
            ],
            'type',
            [
                'label' => 'Status',
                'value' => function ($model) {
                    return \app\models\Task::find()->statuses()[$model->status];
                },
                'attribute' => 'status'
            ],
            'description:ntext',
            'created_at',
            'beginning',
            'ending',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
